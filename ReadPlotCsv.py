import matplotlib.pyplot as plt
from os import listdir
import numpy as np

filepath = str(input("Directory: "))

def find_csv_filenames( path_to_dir, suffix=".csv" ):
    filenames = listdir(path_to_dir)
    return [ path_to_dir+filename for filename in filenames if filename.endswith( suffix ) ]

stripnumber = [i+1 for i in range(len(find_csv_filenames(filepath)))]
length      = list()
width       = list()
area        = list()
ica         = list()
fitmatch    = list()
for count, csv in enumerate(find_csv_filenames(filepath)):
    with open(r""+csv, "r") as file:
        lines = file.readlines()
        data = lines[2]
        entries = data.split(", ")
        length.append(float(entries[1]))
        width.append(float(entries[2]))
        area.append(float(entries[3]))
        ica.append(float(entries[4]))
        fitmatch.append(float(entries[5]))

data  = np.array([np.array(stripnumber), np.array(length), np.array(width), np.array(area), np.array(ica), np.array(fitmatch)])
names = ["N", "L", "W", "A", "ICA", "M"]
fig, axs = plt.subplots(6,6)
for i in range(6):
    for j in range(6):
        axs[i,j].plot(data[i], data[j])
        axs[i,j].set_title(names[i]+" / "+names[j])
        axs[i,j].set_xticks([np.min(data[i]), np.max(data[i])])
        axs[i,j].set_yticks([np.min(data[j]), np.max(data[j])])
plt.show()
plt.close()




